//
//  Student.swift
//  ManageStudentApp
//
//  Created by Le Nguyen Hoang Cuong on 10/21/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

class Student {
    var Name:String!
    var Gender: String!
    var Birthday: Date!
    var ClassName: String!
    var OtherInfo: String?
    var Image: UIImage
    init (_ name: String!, _ gender: String!, _ birthday: Date!, _ classname: String!, _ otherinfo: String?, _ image: UIImage) {
        Name = name
        Gender = gender
        Birthday = birthday
        ClassName = classname
        OtherInfo = otherinfo
        Image = image
    }
    init (_ name: String!, _ gender: String!, _ birthday: Date!, _ classname: String!, _ image: UIImage) {
        Name = name
        Gender = gender
        Birthday = birthday
        ClassName = classname
        OtherInfo = ""
        Image = image
    }
}
