//
//  ViewController.swift
//  ManageStudentApp
//
//  Created by Le Nguyen Hoang Cuong on 10/21/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var StudentImage: UIImageView!
    @IBOutlet weak var NameTxt: UITextField!
    @IBOutlet weak var GenderPicker: UIPickerView!
    @IBOutlet weak var BirthPicker: UIDatePicker!
    @IBOutlet weak var OtherTxt: UITextField!
    @IBOutlet weak var ClassTxt: UITextField!
    @IBAction func AddAction(_ sender: UIButton) {
        if(NameTxt.text == "" || ClassTxt.text == "") {
            let alert = UIAlertController(title: "Cảnh báo", message: "Bạn điền thiếu thông tin", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    let Gender = ["male", "female"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        StudentImage.image = #imageLiteral(resourceName: "graduate-drawing-5.png")
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        StudentImage.isUserInteractionEnabled = true
        StudentImage.addGestureRecognizer(tapGestureRecognizer)
        self.GenderPicker.dataSource = self
        self.GenderPicker.delegate = self
    }
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        image.allowsEditing = false
        
        self.present(image, animated: true) {
            
        }
        
        // Your action
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (NameTxt.text != "" && ClassTxt.text != "") {
            if(segue.identifier == "ShowTable") {
                let gender = pickerView(GenderPicker, titleForRow: GenderPicker.selectedRow(inComponent: 0), forComponent: 0)
                var NewStudent: Student!
                if OtherTxt.text != "" {
                NewStudent = Student(NameTxt.text, gender, BirthPicker.date, ClassTxt.text, OtherTxt.text, StudentImage.image!)
                }
                else {
                NewStudent = Student(NameTxt.text, gender, BirthPicker.date, ClassTxt.text, StudentImage.image!)
                }
                StudentList.append(NewStudent)
            }
        }
    }

}
extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Gender.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Gender[row]
    }
}
extension ViewController: UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            StudentImage.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
}
