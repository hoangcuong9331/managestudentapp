//
//  StudentListViewCell.swift
//  ManageStudentApp
//
//  Created by Le Nguyen Hoang Cuong on 10/21/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

class StudentListViewCell: UITableViewCell {
    @IBOutlet weak var StudentImage: UIImageView!
    @IBOutlet weak var NameTxt: UILabel!
    @IBOutlet weak var genderTxt: UILabel!
    @IBOutlet weak var birthTxt: UILabel!
    @IBOutlet weak var classTxt: UILabel!
    @IBOutlet weak var otherTxt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
