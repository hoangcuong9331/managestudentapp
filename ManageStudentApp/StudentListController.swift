
//
//  StudentListController.swift
//  ManageStudentApp
//
//  Created by Le Nguyen Hoang Cuong on 10/21/18.
//  Copyright © 2018 Le Nguyen Hoang Cuong. All rights reserved.
//

import UIKit

var StudentList = [Student]()

class StudentListController: UIViewController {
    
    @IBOutlet weak var StudentTbView: UITableView!
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        self.StudentTbView.isEditing = !self.StudentTbView.isEditing
        sender.title = (self.StudentTbView.isEditing) ? "Done" : "Edit"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

extension StudentListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StudentList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = StudentTbView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StudentListViewCell
        cell.StudentImage.image = StudentList[indexPath.row].Image
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: StudentList[indexPath.row].Birthday)
        
        cell.birthTxt.text = myString
        cell.classTxt.text = StudentList[indexPath.row].ClassName
        cell.genderTxt.text = StudentList[indexPath.row].Gender
        cell.NameTxt.text = StudentList[indexPath.row].Name
        cell.otherTxt.text = StudentList[indexPath.row].OtherInfo
        
        return (cell)
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObjTemp = StudentList[sourceIndexPath.item]
        StudentList.remove(at: sourceIndexPath.item)
        StudentList.insert(movedObjTemp, at: destinationIndexPath.item)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            StudentList.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
